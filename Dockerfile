# syntax=docker/dockerfile:1
FROM alpine:latest

ENV TZ=Europe/London
RUN apk add --no-cache tzdata \
    && rm -rf /var/cache/apk/* /var/tmp/* \
    && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo $TZ > /etc/timezone

RUN apk add --no-cache iptables wireguard-tools openssh \
    && rm -rf /var/cache/apk/* /var/tmp/*

COPY <<EOF /usr/local/bin/wireguard.sh
#!/bin/sh
set -e

/sbin/iptables -I INPUT -i eth0 -m conntrack --ctstate NEW -p udp --dport 51820 -j ACCEPT

/sbin/iptables -I FORWARD -i wg0 -j ACCEPT
/sbin/iptables -I FORWARD -i wg0 -o eth0 -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
/sbin/iptables -I FORWARD -i eth0 -o wg0 -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

/sbin/iptables -t nat -A POSTROUTING -s 172.16.0.0/24 -o eth0 -j MASQUERADE

/sbin/iptables -A OUTPUT -o wg0 -j ACCEPT

exec /usr/bin/wg-quick up wg0
EOF

RUN chmod +x /usr/local/bin/wireguard.sh

CMD [ "sh", "-c", "/usr/local/bin/wireguard.sh ; /usr/bin/tail -f /dev/null" ]



# docker builder prune -af \
#     && docker system prune -af \
#     && docker volume prune -f \
#     && docker image prune -af

# docker login
# DOCKER_BUILDKIT=1 docker build --no-cache -t 00x56/docker-wireguard:latest .
# docker push 00x56/docker-wireguard:latest

# docker run --interactive --tty --cap-add=NET_ADMIN --env TZ=Europe/Moscow --volume /etc/localtime:/etc/localtime:ro --volume $(pwd)/conf/wireguard/server/wg0.conf:/etc/wireguard/wg0.conf --volume $(pwd)/conf/wireguard/client:/etc/wireguard/client --workdir /etc/wireguard --publish 51820:51820/udp --detach --restart unless-stopped 00x56/docker-wireguard
# docker exec --interactive --tty 7809913082a3 /bin/sh
